import angular from 'angular';
import UserService from './user.service';

let userModule = angular.module('app.services.user', [])
  .service('User', UserService);

export default userModule;
