import angular from 'angular';
import Home from './home/home';
import About from './about/about';

let pagesModules = angular.module('app.states', [
  Home.name,
  About.name,
]);

export default pagesModules;
