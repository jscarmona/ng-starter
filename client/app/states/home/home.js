import angular from 'angular';
import template from './home.html';
import controller from './home.controller';

let homeModule = angular.module('app.states.home', [])
  .run((Router) => {
    'ngInject';

    Router.configureState('home', {
      template,
      controller,
      controllerAs: 'vm',
      url: '/',
      resolve: {
        pageData: () => 'Data'
      }
    });
  });

export default homeModule;
