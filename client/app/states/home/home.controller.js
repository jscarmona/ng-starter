class HomeController {
  /* @ngInject */
  constructor(pageData) {
    this.name = pageData || 'home';
  }
}

export default HomeController;
