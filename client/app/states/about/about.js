import angular from 'angular';
import uiRouter from 'angular-ui-router';
import controller from './about.controller';
import template from './about.html';

let aboutModule = angular.module('app.states.about', [
    uiRouter
  ])
  .config(($stateProvider) => {
    'ngInject';

    $stateProvider.state('about', {
      controller,
      template,
      url: '/about',
      controllerAs: 'vm',
    });
  });

export default aboutModule;
