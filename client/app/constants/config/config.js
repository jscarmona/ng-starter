import angular from 'angular';
import ConfigConstant from './config.constant';

let configModule = angular.module('app.constants.config', [])
  .constant('config', ConfigConstant);

export default configModule;
