import angular from 'angular';
import Config from './config/config';
import Lodash from './lodash/lodash';
import Moment from './moment/moment';

let constantsModule = angular.module('app.constants', [
  Config.name,
  Lodash.name,
  Moment.name,
]);

export default constantsModule;
