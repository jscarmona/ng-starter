import angular from 'angular';
import _ from 'lodash';

let lodashModule = angular.module('app.constants.lodash', [])
  .constant('_', _);

export default lodashModule;
