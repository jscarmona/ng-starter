import angular from 'angular';
import moment from 'moment';

let momentModule = angular.module('app.constants.moment', [])
  .constant('moment', moment);

export default momentModule;
