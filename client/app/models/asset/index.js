'use strict';

export default class Asset {
  constructor(asset) {
    this._asset = asset;
  }

  get id() {
    return this._asset.id;
  }

  get title() {
    return this._asset.title;
  }

  set title(title) {
    this._asset.title = title;
  }
}

class Asset {}
