class Router {
  constructor($stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
    'ngInject';

    this.$stateProvider = $stateProvider;

    $locationProvider.html5Mode(true);
    $urlMatcherFactoryProvider.strictMode(false);
    $urlRouterProvider.otherwise('/404');
  }

  configureStates(states) {
    this.$stateProvider.state(name, config);
  }

  configureState(name, config) {
    this.$stateProvider.state(name, config);
  }

  $get() {
    return {
      configureStates: this.configureStates.bind(this),
      configureState: this.configureState.bind(this),
    };
  }
}

export default Router;
