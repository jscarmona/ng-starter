import angular from 'angular';
import RouterProvider from './router.provider';

let routerModule = angular.module('app.providers.router', [])
  .provider('Router', RouterProvider);

export default routerModule;
