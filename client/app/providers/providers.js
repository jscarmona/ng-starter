import angular from 'angular';
import RouterProvider from './router/router';

let providersModule = angular.module('app.providers', [
  RouterProvider.name
]);

export default providersModule;
