import angular from 'angular';
import LogConfig from './log.config';

let logModule = angular.module('app.configs.log', [])
  .config(LogConfig);

export default logModule;
