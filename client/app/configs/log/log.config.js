function log($logProvider) {
  'ngInject';

  $logProvider.debugEnabled(true);
}

export default log;
