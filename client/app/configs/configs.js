import angular from 'angular';
import Log from './log/log';

let configsModule = angular.module('app.configs', [
  Log.name
]);

export default configsModule;
