import angular from 'angular';
import angularComponent from 'angular-component';
import uiRouter from 'angular-ui-router';
import Components from './components/components';
import Configs from './configs/configs';
import Constants from './constants/constants';
import Factories from './factories/factories';
import Filters from './filters/filters';
import Providers from './providers/providers';
import Services from './services/services';
import States from './states/states';

angular.module('app', [
    uiRouter,
    Components.name,
    Configs.name,
    Constants.name,
    Factories.name,
    Filters.name,
    Providers.name,
    Services.name,
    States.name,
  ]);
