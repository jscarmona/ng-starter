import angular from 'angular';
import App from './app/app';
import Navbar from './navbar/navbar';
import Hero from './hero/hero';

let componentsModules = angular.module('app.components', [
  App.name,
  Navbar.name,
  Hero.name,
]);

export default componentsModules;
