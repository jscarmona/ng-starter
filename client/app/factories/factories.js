import angular from 'angular';

let factoriesModule = angular.module('app.factories', [
  //
]);

export default factoriesModule;
