'use strict';

// import ignite from 'gulp-ignite';
//
// // Overwrite configuration
// ignite.config({
//   paths: {}
// });
//
// // Whitelist tasks to make available
// // pass optional 2nd argument as config override for individual tasks
// ignite.only([
//   'copy:index',
//   'webpack'
// ], {
//   webpack: {},
//   'copy:index': {},
// });
//
// ignite.except([], {});
//
// // Use all tasks
// ignite.all();



import gulp     from 'gulp';
import webpack  from 'webpack-stream';
import path     from 'path';
import sync     from 'run-sequence';
import serve    from 'browser-sync';
import rename   from 'gulp-rename';
import template from 'gulp-template';
import fs       from 'fs';
import yargs    from 'yargs';
import lodash   from 'lodash';
import resolve  from './gulp/resolve';
import config   from './gulp/config';

import generator from './gulp/tasks/ng-generator';

generator();

let reload = () => serve.reload();

gulp.task('copy:index', () => {
  return gulp.src(resolve.toClient('index.html'))
    .pipe(gulp.dest(config.paths.output));
});

gulp.task('webpack', () => {
  return gulp.src(config.paths.entry)
    .pipe(webpack(require('./webpack.config')))
    .pipe(gulp.dest(config.paths.output));
});

gulp.task('serve', () => {
  serve({
    port: process.env.PORT || 3000,
    open: false,
    server: { baseDir: resolve.toPublic() }
  });
});

gulp.task('watch', () => {
  let allPaths = [].concat([config.paths.js], config.paths.html);

  gulp.watch(allPaths, ['webpack', reload]);
  gulp.watch(resolve.toClient('index.html'), ['copy:index']);
});

gulp.task('default', (done) => {
  sync('copy:index', 'webpack', 'serve', 'watch', done);
});
