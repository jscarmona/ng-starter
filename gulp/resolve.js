'use strict';

import path from 'path';
import config from './config';

export default {
  toPublic(glob = '') {
    return path.join(config.paths.output, glob);
  },

  toClient(glob = '') {
    return path.join(config.paths.root, glob);
  },

  toApp(glob = '') {
    return path.join(config.paths.app, glob);
  },

  toGeneratorTemplates(glob = '') {
    return path.join(config.paths.blankTemplates, glob);
  },

  toComponents(glob = '') {
    return path.join(config.paths.components, glob);
  }
};
