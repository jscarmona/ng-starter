'use strict';

import path from 'path';

export default {
  paths: {
    root: 'client',
    app: 'client/app',
    components: 'client/app/components',
    js: 'client/app/**/*!(.spec.js).js',
    html: 'client/app/**/*.html',
    entry: 'client/app/app.js',
    output: 'public',
    blankTemplates: '.generator/component/**/*.**'
  }
};
