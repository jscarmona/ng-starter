'use strict';

import gulp from 'gulp';
import path from 'path';
import _ from 'lodash';
import template from 'gulp-template';
import yargs from 'yargs';
import rename from 'gulp-rename';
import resolve from '../../resolve';
import config from '../../config';

export default (options = {}) => {
  gulp.task('ng-generate', () => {
    let type = yargs.argv.component || 'component';
    let name = yargs.argv.name;
    let parentPath = yargs.argv.parent || '';
    let destPath = path.join(resolve.toComponents(), parentPath, name);

    return gulp.src(options.src || path.resolve(__dirname, `.templates/${type}/**/*`))
      .pipe(template({
        name: name,
        upCaseName: _.capitalize(name)
      }))
      .pipe(rename((path) => {
        path.basename = path.basename.replace('temp', name);
      }))
      .pipe(gulp.dest(destPath));
  });
}
